import 'bootstrap/dist/css/bootstrap.min.css';
import $ from "jquery";
import DiceComponent from './components/DiceComponent';
function App() {
  return (
    <div >
      <DiceComponent/>
    </div>
  );
}

export default App;
