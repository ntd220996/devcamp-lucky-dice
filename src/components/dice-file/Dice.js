import { Component } from "react";
import  dice  from '../../assets/images/dice.png'
import Dice1 from '../../assets/images/1.png'
import Dice2 from '../../assets/images/2.png'
import Dice3 from '../../assets/images/3.png'
import Dice4 from '../../assets/images/4.png'
import Dice5 from '../../assets/images/5.png'
import Dice6 from '../../assets/images/6.png'

const styleImgDice = {
    height: '320px',
    width: '350px',
    marginTop: '10px'
}

class Dice extends Component {
    render() {
        const {randomDice} = this.props
        const Dice = () =>  {
            switch (randomDice) {
              case 1:
                return Dice1;
              case 2:
                return Dice2;
              case 3:
                return Dice3;
              case 4:
                return Dice4;
              case 5:
                return Dice5;
              case 6:
                return Dice6;
              default:
                return dice;
            }
          };
        return(
            <div className="text-center">
                <img className="img-thumbnail " src={Dice(randomDice)} style={styleImgDice} />
            </div>
        )
    }
}

export default Dice