import { Component } from "react";
import Dice from "./dice-file/Dice";


class DiceComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dice: 0
        }
    }

    onThrowClick = () => {
        console.log('Ném');

        let randomDice =  Math.floor(Math.random() * 6) + 1;

        this.setState({
            dice: randomDice
        })
    }

    render() {
        return(
            <div className="text-center">
                <h1>Lucky Dice</h1>
                <br/>
                <button onClick={this.onThrowClick} className="btn btn-primary"> Throw </button>
                <Dice randomDice={this.state.dice}/>
            </div>
        )
    }
}

export default DiceComponent